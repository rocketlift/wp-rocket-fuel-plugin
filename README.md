# One Time Scripts

One-time scripts are just that: scripts that run to make changes to an environment, just once. This is useful for scripting actions that are tedious. Advantages are testability (for quality assurance) and speed (to minimize downtime). The tool also runs from the WordPress admininstrative GUI, so it doesn't have other dependencies such as `ssh`, or `wp-cli`. (When those are availabile, they are often better to use. But when they aren't, there's this here One Time Scripts deal.)

To register a function to run as a one time script, hook it to the `rli_one_time_scripts` action hook in some isolated place, such as a site-specific plugin. Then to run the scripts, log in as an administrator, install and activate Rocket Fuel, and go to Dashboard -> Tools -> RLI Fuel Tool. Then, follow the prompts.

## Example Code Usage

Just add the function you would like to run to the `rli_one_time_scripts` action hook.

Like such:

```
<?php
function this_stuff_had_better_only_ever_be_done_once() {
	add_option( 'truth', 'This tool is really cool' );
}

add_action( 'rli_one_time_scripts', 'this_stuff_had_better_only_ever_be_done_once' );
```

# Replace Shortcodes Tool

_Prelude: Admitedly, the interface for this is a bit janky._

This allows you to pass a custom set of search/replace rules for shortcodes in WP content. The individual rules should be in an isolated place, such as a site-specific plugin. They need to be added to an array that is passed to filter.

This elaborate contraption is triggered from the WP GUI: To run the scripts, log in as an administrator, install and activate Rocket Fuel, and go to Dashboard -> Tools -> RLI Fuel Tool. Then, press the "Replace Shortcodes" button.

In order to prevent the script from timing out the PHP process on a large dataset, we cap the number of replacements per run. In this early version of the tool, there's no spiffy AJAX or anything. We've taken an iterative "click the button several times" approach instead, because good enough. 

So, it can be necessary for you to click the button multiple times to finish all replacements.

## Creating Replacement Rules

This uses a similar mechanism to WordPress's internal shortcode handling. Normally WordPress looks for shortcodes when preparing content for display, identifies shortcodes, and uses shortcode callbacks to output shortcode content instead of the shortcode itself. 

In this case, we replace shortcode callbacks with our replacement shortcodes, invoke the shortcode handling mechanism, and cause it to overwrite post content in the database according to replacement rules.

Shortcode replacement callbacks look just like shortcode callbacks used to register shortcodes: 

Three parameters are passed to the shortcode replacement callback function. You can choose to use any number of them including none of them.

 * `$atts` - an associative array of attributes, or an empty string if no attributes are given
 * `$content` - the enclosed content (if the shortcode is used in its enclosing form)
 * `$tag` - the shortcode tag, useful for shared callback functions

Here is an example of a replacement callback:
```PHP
// Create a replacement rule
// For this example, assume we are migrating away from using a plugin that
// adds a `[div]` shortcode. We need to replace instances of "[div]…[/div]"
// in content with "<div>…</div>".
function replace_div_shortcode( $args, $content ) {
	$output = "<div";

	if ( ! is_empty( $args['id'] ) ) {
		$id = $args['id'];
		$output .= " id=$id";
	}

	if ( ! is_empty( $args['class'] ) ) {
		$class = $args['class'];
		$output .= " class=$class";
	}

	$output .= ">";
	$output .= $contnet;
	$output .= "</div>";

	return $output;
}
```

## Packaging Replacement Rules

You will package replacement rules as callback functions in an array. The array will be keyed by shortcode, with the callback functions' names as values. 

E.g.:

```PHP
array(
	'a_shortcode' => 'replacement_callback',
	'another_shortcode' => 'another_replacement_callback'
);
```
You will then pass your array to the `rli_fuel_replace_shortcodes_rules_array` filter. Each matching shortcode will be replaced with what the `replacement_callback` function returns. 

## Configurable Options in Code

Note two filters for configuring shortcode replacement behavior. These are useful during debugging of your search/replace rules: 

1. `rli_fuel_replace_shortcodes_dry_run`, which defaults to `true`, will simulate shortcode output instead of carrying it out.
2. `rli_fuel_replace_shortcodes_verbose`, which defaults to `false`, will show more output if enabled. 


## A Complete Example of Use

```PHP
// Disable dry-run mode when you're done with debugging
add_filter( 'rli_fuel_replace_shortcodes_dry_run', '__return_false' );

// Enable verbose mode for debugging
add_filter( 'rli_fuel_replace_shortcodes_verbose', '__return_true' );

// Create a replacement rule
// For this example, assume we are migrating away from using a plugin that
// adds a `[div]` shortcode. We need to replace instances of "[div]…[/div]"
// in content with "<div>…</div>".
function replace_div_shortcode( $args, $content ) {
	$output = "<div";

	if ( ! is_empty( $args['id'] ) ) {
		$id = $args['id'];
		$output .= " id=$id";
	}

	if ( ! is_empty( $args['class'] ) ) {
		$class = $args['class'];
		$output .= " class=$class";
	}

	$output .= ">";
	$output .= $contnet;
	$output .= "</div>";

	return $output;
}

// Prepare a function that returns an array of your rules
function my_replacement_rules() {
	$rules_array = array(
		'replace_div_shortcode'
	);
	return $rules_array;
}

// Pass the array to the `rli_fuel_replace_shortcodes_rules_array` filter
add_filter( 'rli_fuel_replace_shortcodes_rules_array', 'my_replacement_rules' );
```

To activate replacement, remember to trigger this through the WP admin GUI under Tools > Rocket Fuel.
