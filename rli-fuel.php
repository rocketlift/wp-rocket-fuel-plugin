<?php
/*
Plugin Name: Rocket Lift Fuel
Plugin URI: http://rocketlift.com
Description: Rocket Lift WP Fuel Plugin
Author: Kevin Lenihan, Matthew Eppelsheimer, Rocket Lift
Version: 1.1.4
Requires at least: 3.5
Author URI: http://rocketlift.com
*/

/*  Copyright 2013 Rocket Lift (email : software@rocketlift.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// If called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

function rli_fuel_init() {
	require_once( plugin_dir_path( __FILE__ ) . 'inc/list-used-shortcodes.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'inc/replace-shortcodes.php' );
}
add_action( 'admin_init', 'rli_fuel_init' );

/*
 * Returns an array containing infomation on the search query
 * @array['first'] = the first post count of the search query listed on the current page.
 * @array['last'] = the last post count of the search query listed on the current page.
 * @array['total'] = the total posts found in the query
 * 
 * Example: 
 *   $arr = rli_get_search_result_nums();
 *   echo 'Search Results: ' . $arr['first'] . ' - ' . $arr['last'] . ' out of ' . $arr['total'];
 * Output:
 * 	Search Results: 1 - 10 out of 46 
 */

function rli_get_search_result_nums(){
	global $wp_query;
	$page_count = $wp_query->query_vars['paged'];
	if ( 0 == $page_count ) $page_count++;
	
	$first = $wp_query->query_vars['posts_per_page'] * ( $page_count - 1 );
	$last = $first++ + $wp_query->post_count ;
	$total = $wp_query->found_posts;
	return array( 'first' => $first, 'last' => $last, 'total' => $total );
}

/**
 * Prints out the Launced By Rocket Lift footer tab
 */

function rli_footer_right() {
	echo 'Launched by <a href="http://rocketlift.com">Rocket Lift</a>';
}

/**
 * One Time Scripts system
 */

// Setup admin AJAX
add_action( 'admin_init', function(){
	wp_register_script( 'rli-ots-script', plugins_url( '/js/one-time-scripts.js', __FILE__ ), array( 'jquery' ) );
	wp_enqueue_script( 'rli-ots-script' );
} );

function rli_run_one_time_scripts() {
	global $rli_ots_error;
	$new_scripts = rli_get_on_deck_scripts();
	$run_scripts = get_option( 'rli_ots_list' );
	
	try {
		// do_action doesn't fail gracefully, so we have to call the functions ourselves
		foreach ( $new_scripts as $funct ) {
			if ( ! function_exists( $funct ) ) {
				// function does not exist so don't do anything more
				throw new Exception( 'Call to undefined function ' . $funct );	
				return;
			}
			$funct();
			$used_scripts[] = $funct;
			update_option( 'rli_ots_list', $used_scripts );
		}
	} catch ( Exception $e ) {
		// Invalid Function: Get Us Outta Here!
		$rli_ots_error = $e;
		return;
	}
}

/*
 * One time script Ajax Hooks 
 */

add_action( 'wp_ajax_rli_run_one_time_scripts', function() {
	rli_run_one_time_scripts();
	die(rli_get_script_status());
});

add_action( 'wp_ajax_rli_ots_reset_option', function(){
	delete_option( 'rli_ots_list' );
	die(rli_get_script_status());
});

add_action( 'wp_ajax_rli_ots_approve_user', function(){
	update_option( 'rli_ots_user', $_REQUEST['user'] );
	die();
});

add_action( 'wp_ajax_rli_ots_unapprove_user', function(){
	delete_option( 'rli_ots_user' );
	die();
});

function rli_script_page() {
	if ( ! current_user_can('manage_options')) {  
		wp_die('You do not have sufficient permissions to access this page.');  
	}
	?>
	<div class="wrap">  
        <?php screen_icon('themes'); ?> <h2>RLI Fuel Tools</h2>
		<?php do_action( 'rli_fuel_debug' ); ?>
		<?php do_action( 'rli_fuel_admin_page_content' ); ?>
    </div>  
	<?php
}

add_action( 'admin_menu', function() { // AJAX hook
	add_submenu_page('tools.php', 'RLI Fuel Tools', 'RLI Fuel Tool', 'manage_options', 'rli-fuel-tools', 'rli_script_page');
});


function rli_one_time_script_admin_page_content() { ?>
	<h3>One Time Script Tool</h3>
	<?php
	// if there is no approved user we need to set one
	if ( ! $approved_user = get_option( 'rli_ots_user' ) ) {
		rli_ots_setup_approved_user();
		return;
	}

	if ( get_current_user_id() != $approved_user ) { ?>
		<p><?php printf( __( 'You are not the approved user for this tool, only %s can. Move along now; nothing to see here.'), get_userdata( $approved_user )->display_name ); ?></p>
	<?php } else { ?>
		<a class="button" onclick="one_time_scripts()">Run One Time Scripts</a>
		<a class="button" onclick="reset_ots_option()">Reset Checked Script List</a>
		<a class="button" onclick="unapprove_user()">Unapprove Assign Yourself</a><span> </span><span class="one-time-scripts"></span>
		<div id="one-time-script-results">
			<?php rli_get_script_status(); ?>
		</div>
	<?php }
}
add_action( 'rli_fuel_admin_page_content', 'rli_one_time_script_admin_page_content' );

function rli_get_script_status() { 
	global $rli_ots_error;
	$new_scripts = rli_get_on_deck_scripts();
	$run_scripts = get_option( 'rli_ots_list' );
	?><div><?php
	if ( ! empty( $new_scripts ) ) :?>
	<ul id="on-deck-scripts" style="float:left;">
		<li><strong>On-Deck Scripts</strong></li>
		<?php foreach ( $new_scripts as $script ) {
			echo '<li>' . $script . '</li>';
		} ?>
	</ul>
	<?php endif;
	if ( $run_scripts ) : ?>
	<ul id="run-scripts" style="float:left;">
		<li><strong>Run Scripts</strong></li>
		<?php foreach( $run_scripts as $script ) {
			echo '<li>' . $script . '</li>';
		} ?>
	</ul>
	<?php endif;
	if ( $rli_ots_error ) : ?>
	</div>
	<div style="clear:both;">
	<h3>Error Encountered</h3>
	<pre><?php echo $rli_ots_error; ?></pre></div>
	<?php endif;
}

function rli_get_on_deck_scripts() {
	global $wp_filter;
	
	$all = array();

	if ( ! isset( $wp_filter['rli_one_time_scripts'] ) ) {
		return false;
	}
	
	// NOTE: This was built using the filter structure as of August 2014
	//       This was changed with WP 4.7; see https://make.wordpress.org/core/2016/09/08/wp_hook-next-generation-actions-and-filters/
	//       Fortunately, (quoted from the above:) "The WP_Hook object implements the ArrayAccess and IteratorAggregate interfaces so
	//       that, while it’s not recommended, you may continue to iterate over callbacks in $wp_filter or directly retrieve priorities
	//       from the callback array".
	foreach( $wp_filter['rli_one_time_scripts'] as $priority => $functs ) {
		foreach( $functs as $name => $specs ) {
			$all[] = $name;
		}
	}
	
	$run_scripts = get_option( 'rli_ots_list' );
	if ( ! $run_scripts ) {
		return $all;
	}
	$unrun = array_diff( $all, $run_scripts );
	return $unrun;
}

function rli_ots_setup_approved_user() { 
	$all_users = get_users();
	$specific_users = array();

	foreach( $all_users as $user ){
		if( $user->has_cap( 'manage_options' ) ){
		    $manager_users[] = $user;
		}

	}
	?>
	<h3>There is no approved user for this tool. Please select one now. NOTE: If you select someone other than yourself, you will not be able to undo that.</h3>
	<select name="user" id="user">
	<?php foreach( $manager_users as $user ) {
		$selected = '';
		if ( get_current_user_id() == $user->ID ) {
			$selected = ' selected';
		}
		echo '<option value="' . $user->ID . '"' . $selected . '>' . $user->display_name . '</option>';
	} ?>	
	</select>
	<a class="button" onclick="approve_user()">Approve User</a>
<?php }


/**
 * Utility script to update Network Activated plugins to be activated for each blog.
 * Useful as a precursor step when disabling Multisite.
 * See http://kovshenin.com/2012/how-to-network-deactivate-a-wordpress-plugin/
 * See also http://www.poststat.us/completely-disable-multisite/
 *
 * @use-with rli_register_one_time_script().
 */

function switch_network_activated_plugins_to_site_activated() {
	global $wpdb;

	// Duplicate functionality in `get_site_options` when is_multisite() == true, so we don't have to assume that.
	// Simplified quite a bit from basis function.
	function get_site_option_with_multisite( $option ) {
		global $wpdb;
		$siteid = apply_filters( 'get_site_option_with_multisite_siteid', 1 );
		if ( !isset($value) || (false === $value) ) {
			$query = $wpdb->prepare("SELECT meta_value FROM wp_sitemeta WHERE meta_key = %s AND site_id = %d", $option, $siteid );
			$row = $wpdb->get_row( $query );

			// Has to be get_row instead of get_var because of funkiness with 0, false, null values
			if ( is_object( $row ) ) {
				$value = $row->meta_value;
				$value = maybe_unserialize( $value );
			} else {
				$value = false; // error
			}
		}
		return $value;
	}

	// Sitewide active plugins
	$network_active_plugins = array_keys( get_site_option_with_multisite( 'active_sitewide_plugins' ) );

	// For debugging
	// Blog-specific plugins
	$plugins = get_option( 'active_plugins' );
	echo "<pre>Network active_plugins: ";
	print_r( $network_active_plugins );
	echo "</pre>";
	echo "<pre>active_plugins: ";
	print_r( $plugins );
	echo "</pre>";
	//*/

	$blogs = $wpdb->get_col( "SELECT blog_id FROM $wpdb->blogs;" );
	foreach ( $network_active_plugins as $plugin ) {
		if ( is_multisite() ) {
			deactivate_plugins( $plugin, true, true ); // Prevent calling deactivation hooks, and run network-wide.
			foreach ( $blogs as $id ) {
				switch_to_blog( $id );
				activate_plugin( $plugin, null, false, true ); // Defaults, except prevent calling activation hooks
			}
		} else {
			// We won't network deactivate, because that won't work.
			// We won't loop through all blogs, only the single site that WP recognizes
			activate_plugin( $plugin, null, false, true ); // Defaults, except prevent calling activation hooks
		}
	}
	if ( is_multisite() ) {
		restore_current_blog();
	}

	// For debugging
	printf( "%d plugins activated on %d blogs.", count( $network_active_plugins ), count( $blogs ) );
	if ( ! is_multisite() ) {
		printf( "We didn't network deactivate because multisite is disabled." );
	}
	$network_active_plugins = array_keys( get_site_option_with_multisite( 'active_sitewide_plugins' ) );
	$plugins = get_option( 'active_plugins' );
	echo "<pre>Network active_plugins: ";
	print_r( $network_active_plugins );
	echo "</pre>";
	echo "<pre>active_plugins: ";
	print_r( $plugins );
	echo "</pre>";
	//*/
}

/*
 * Time-saving debugging function
 */
function rli_print_r( $obj, $obj_name = null ) {
	echo "<div class='update-nag'>\n";
	if ( ! empty( $obj_name ) ) {
		echo "<p><strong>Object: $obj_name</strong></p>\n";
	}
	echo "<pre>\n";
	print_r( $obj );
	echo "</pre>\n";
	echo "</div>\n";
}

