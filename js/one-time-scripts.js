function one_time_scripts(){
	jQuery('.one-time-scripts').html('... Running Scripts ...');
	jQuery.ajax({
		type: "POST",
		url: "admin-ajax.php",
		data: {action: 'rli_run_one_time_scripts' },
		success: function(data, textStatus, XMLHttpRequest){
			jQuery('.one-time-scripts').html('<strong>Complete</strong>');
			jQuery('#one-time-script-results').html(data);
			
		},
		error: function(MLHttpRequest, textStatus, errorThrown){
				alert("ERROR: " + MLHttpRequest + textStatus + errorThrown); 
			},
	});
}

function reset_ots_option(){
	jQuery('.one-time-scripts').html('... Reseting List ...');
	jQuery.ajax({
		type: "POST",
		url: "admin-ajax.php",
		data: {action: 'rli_ots_reset_option' },
		success: function(data, textStatus, XMLHttpRequest){
			jQuery('.one-time-scripts').html('<strong>Complete</strong>');
			jQuery('#one-time-script-results').html(data);
		},
		error: function(MLHttpRequest, textStatus, errorThrown){},
	});
}

function approve_user(){
	var user = jQuery( "#user option:selected" ).val();
	jQuery.ajax({
		type: "POST",
		url: "admin-ajax.php",
		data: {action: 'rli_ots_approve_user', user: user },
		success: function(data, textStatus, XMLHttpRequest){
			location.reload();
		},
		error: function(MLHttpRequest, textStatus, errorThrown){},
	});
}

function unapprove_user(){
	jQuery.ajax({
		type: "POST",
		url: "admin-ajax.php",
		data: {action: 'rli_ots_unapprove_user' },
		success: function(data, textStatus, XMLHttpRequest){
			location.reload();
		},
		error: function(MLHttpRequest, textStatus, errorThrown){},
	});
}

