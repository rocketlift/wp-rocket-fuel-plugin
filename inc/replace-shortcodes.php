<?php
/*
 * A system for replacing shortcodes with set content.
 * Useful when preparing to export content to non-WordPress system.
 *
 * @since	v1.1.3
 *
 */

// For now, call directly on the tools page
add_action( 'rli_fuel_admin_page_content', 'rli_fuel_replace_shortcodes' );
// Dry run for replace-shortcodes
// add_filter( 'rli_fuel_replace_shortcodes_dry_run', '__return_false' );
// Verbose output for replace-shortcodes
// add_filter( 'rli_fuel_replace_shortcodes_verbose', '__return_false' );

/**
 * Callable function for shortcode replacement.
 *
 * Replaces shortcodes in posts and reports replacement statistics,
 * unless $dry_run is true, in which case simulates replacing shortcodes.
 * 
 * @uses	_rli_fuel_do_shortcode_replace()
 * @uses	$shortcode_tags
 * @uses	$shortcode_replacement_run_count
 *
 * @param	$rules_array	array	An array showe keys are shortcode tags
 *									and values are replacement rule function
 *									names. E.g.:
 *									```php
 *									array(
 *										'shortcode_tag', 'replacement_function'
 *									);
 *									```
 * @param	boolean		$dry_run 		Whether to actually do replacement. 
 * 											Defaults to false.
 * 
 * @return				true if successful, false if there were errors.
 */

function rli_fuel_replace_shortcodes( $rules_array = array(), $dry_run = true ) {
	global $shortcode_tags;
	global $shortcode_replacement_run_count;
	global $shortcode_replacement_total_count;

	$rules_array = apply_filters( 'rli_fuel_replace_shortcodes_rules_array', $rules_array );

	if ( empty ( $rules_array ) ) {
		$output = "<h4>Error replacing shortcodes</h4>\n";
		$output .= "<p>No replacement rules specified in <code>\$rules_array</code></p>\n";
		echo $output;
		return false;
	}

	$dry_run = apply_filters( 'rli_fuel_replace_shortcodes_dry_run', $dry_run );
	$verbose = apply_filters( 'rli_fuel_replace_shortcodes_verbose', false );

	// Initialize locally-scoped counters
	$shortcode_replacement_total_count = 0;
	

	// Store $shortcode_tags global before hijacking
	$shortcodes_tags_temp = $shortcode_tags;
	$shortcode_tags = $rules_array;

	// Get posts
	$default_args = array(
		'post_type'			=> 'post',
		'posts_per_page'	=> -1
	);

	$query = new WP_Query( apply_filters( 'rli_fuel_replace_shortcodes_query_args', $default_args ) ); 
	
	$checked = get_option( 'rli_fuel_checked_posts' );
	if ( ! $checked ) {
		$checked = array();
	}
	
	$posts_modified = count( $checked );
	$post_modified_this_time = 0;
	
	// Process the posts using helper function
	foreach ( $query->posts as $post ) {
		
		if ( in_array( $post->ID, $checked ) ){
			continue;
		}
		$checked[] = $post->ID;
		
		$content = $post->post_content;
	
		if ( $verbose ) {
			$verbose_output = "<div class='alert'>\n";
			$verbose_output .= "<p><strong>Post content before shortcode replacement:</strong></p>\n";
			$verbose_output .= "<pre>$content</pre>\n";
			$verbose_output .= "</div>\n";
			echo $verbose_output;
		}
	
		$content = _rli_fuel_replace_content( $content );
		
		if ( $verbose ) {
			$verbose_output = "<div class='alert'>\n";
			$verbose_output .= "<p><strong>Post content after shortcode replacement:</strong></p>\n";
			$verbose_output .= "<pre>$content</pre>\n";
			$verbose_output .= "</div>\n";
			echo $verbose_output;
		}
	
		if ( ! $dry_run ) {
			$updated_post_id = wp_update_post( array(
				'ID'			=>	$post->ID,
				'post_content'	=>	$content
			) );

			if ( 0 == $updated_post_id ) {
				// Return false if we failed
				//return false;
				continue;
			}
		}

		$shortcode_replacement_total_count += $shortcode_replacement_run_count;
		$posts_modified++;
		$post_modified_this_time++;
		if ( $post_modified_this_time > 10 ) {
			break;
		}
	} // end foreach

	update_option( 'rli_fuel_checked_posts', $checked );
	// Reset global
	$shortcode_tags = $shortcodes_tags_temp;

	// Report status
	if ( ! empty( $output ) ) {
		echo $output;
		return false;
	}

	if ( $dry_run ) {
		$output = "<h3>Finished <strong>DRY RUN</strong> of replacing shortcodes</h3>\n";
		$output .= "<p>Would have r";
	} else {
		$output = "<h3>Finished replacing shortcodes.</h3>\n";
		$output .= "<p>R";
	}
	$output .= "eplaced $shortcode_replacement_total_count shortcodes in $posts_modified posts.</p>\n";

	echo $output;
	return true;
}

function _rli_fuel_replace_content( $content ) {
	global $shortcode_replacement_run_count; 
	global $shortcode_replacement_total_count;
	
	do {
		$shortcode_replacement_run_count = 0;
		$content = _rli_fuel_do_shortcode_replace( $content );

		if ( $content === false ) {
			$output = "<h3>Error replacing shortcodes</h3>\n";
			$output .= "<p>During work on post ID $post->ID, after replacing $shortcode_replacement_total_count in $posts_modified posts.</p>";
			break;
		}
		$shortcode_replacement_total_count += $shortcode_replacement_run_count;
	
	// if something has changed check for more shortcodes
	} while( $shortcode_replacement_run_count > 0 );
	
	return $content;
}

/**
 * Do not call directly. Helper function for rli_fuel_replace_shortcodes().
 *
 * Take post content and return a string with shortcodes replaced
 * according to custom rules. Increments $shortcode_replacement_run_count. 
 *
 * Takes inspiration from do_shortcode() in wp-includes/shortcodes.php.
 *
 * @since	v1.1.3
 *
 * @uses	do_shortcode_tag()
 * @uses	$shortcode_tags
 * @uses	$shortcode_replacement_run_count
 *
 * @param		object		$content		$post->post_content from a WP_Query
 *
 * @returns 	If successful, (string) of content with shortcode tags replaced.	
 r 				If unsuccessful, (bool) false.
 */
function _rli_fuel_do_shortcode_replace( $content ) {
	global $shortcode_tags;
	global $shortcode_replacement_run_count;

	if ( false === strpos( $content, '[' ) ) {
		$content;
	}

	if ( empty( $shortcode_tags ) || ! is_array( $shortcode_tags ) ) {
		return false;
	}

	$pattern = get_shortcode_regex();

	$content = preg_replace_callback( "/$pattern/s", 'do_shortcode_tag', $content, -1, $shortcode_replacement_run_count );

	return $content;
}

