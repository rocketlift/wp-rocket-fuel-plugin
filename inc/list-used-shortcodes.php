<?php
/*
 * @since	1.2.0
 *
 * @TODO	Set this up to use AJAX instead of running every time the tool page is accessed.
 * @TODO	Set this up to use a batch process to avoid exceeding memory limits on large sites.
 * @TODO	Allow scoping to more than just post_type=post
 */

// User Interface
function rli_list_used_shortcodes_admin_page_content() { ?>
	<h3>List Used Shortcodes</h3>
	<?php /* <a class="button" onclick="list_shortcodes()">Create Shortcode List</a> */ ?>
	<?php rli_fuel_shortcodes_used_list_process();
}
add_action( 'rli_fuel_admin_page_content', 'rli_list_used_shortcodes_admin_page_content' );

function rli_fuel_shortcodes_used_list_process() {
	global $shortcodes_used_helper;
	global $shortcode_tags;
	$shortcodes_used = array();

	// Settings
	$posts_at_a_time = -1;

	// Get posts
	$query = new WP_Query( array(
		'post_type' => 'post',
		'posts_per_page' => $posts_at_a_time
	) );

	// Process the posts
	foreach ( $query->posts as $post ) {
		$shortcodes_used_helper = array();
		do_shortcode_find( $post->post_content );
		$shortcodes_used = array_merge( $shortcodes_used, $shortcodes_used_helper ); 
	}
	$shortcodes_used = array_unique( $shortcodes_used );

	?><h4>Searched <?php echo count( $query->posts ); ?> posts and found:</h4>
	<ul>
		<?php foreach( $shortcodes_used as $shortcode ) {
			echo "<li>$shortcode</li>";
		} ?>
	</ul>
	<h4>Searched for</h4>
	<ul>
		<?php $shortcode_tag_names = array_keys( $shortcode_tags );
		foreach( $shortcode_tag_names as $shortcode ) {
			echo "<li>$shortcode</li>";
		} ?>
	</ul>
	<?php
}

/**
 * Search content for shortcodes.
 *
 * Based on do_shortcode() in wp-includes/shortcode.php.
 *
 * @uses $shortcode_tags
 * @uses get_inclusive_shortcode_regex() 
 *
 * @param string $content Content to search for shortcodes
 * @return array list of shortcode tags used in content.
 */
function do_shortcode_find( $content ) {
	$pattern = get_shortcode_regex();
	preg_replace_callback( "/$pattern/s", 'do_shortcode_find_tag', $content );
}

/**
 * Regular Expression callable for do_shortcode_find() for calling shortcode hook.
 * Based on do_shortcode_tag() in wp-includes/shortcodes.php.
 *
 * @see get_shortcode_regex for details of the match array contents.
 *
 * @uses $shortcode_tags
 *
 * @param array $m Regular expression match array
 * @return null.
 */
function do_shortcode_find_tag( $m ) {
	global $shortcodes_used_helper;
	$shortcodes_used_helper[] = $m[2];
	if ( isset( $m[5] ) ) {
		do_shortcode_find( $m[5] );
	}
}

